const express = require("express");
const router = express.Router();
const errorPath = require("path");
const staticAssets = errorPath.join(__dirname, "../public");

router

	.get("*", (req, res) => {
		res.statusCode = 404;
		res.sendFile(staticAssets + "/error.html");
	})	

module.exports = router;