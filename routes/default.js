const express = require("express");
const router = express.Router();
const amshome = require("../controllers/amshome"); 
const renovate = require("../controllers/renovate"); 


router
	.get("/", (req, res) => {
		
		res.render("default", {
			page: "home",
			student: "09003096",
			header: "Welcome to AMS@Home",
			details: "This is a serverless replica of AMS that is designed to facilitate the students to enhance thier programming skills. This system requires several modules up and running for better experience for the user therefore it is necessary to check if all the required modules are up and running.",
			button: "Inspect AMS@Home",
			action: "/",
			feedback: " >"
		});
	})

	.post("/", (req, res) => {
		amshome.inspect(function(log){

			res.render("default", {
				page: "home",
				student: "09003096",
				header: "Welcome to AMS@Home",
				details: "This is a serverless replica of AMS that is designed to facilitate the students to enhance thier programming skills. This system requires several modules up and running for better experience for the user therefore it is necessary to check if all the required modules are up and running.",
				button: "Inspect AMS@Home",	
				action: "/",
				feedback: log
			});
		});	
	})


	.get("/update", (req, res) => {
		res.render("default", {
			page: "update",
			student: "09003096",
			header: "Update Module",
			details: "This is an update module for AMS@Home that updates the list of exercises. This module needs to be run every week to update the list of practice weekly exercises, which is generated as a requriement for the unit CAB202, Microprocessors and Digital Systems.",
			button: "Update AMS@Home",
			action: "/update",
			feedback: " >"
		});
	})

	.post("/update", (req, res) => {
		var log = renovate.getExercises(function(){
			res.render("default", {
			page: "update",
			student: "09003096",
			header: "Update Module",
			details: "This is an update module for AMS@Home that updates the list of exercises. This module needs to be run every week to update the list of practice weekly exercises, which is generated as a requriement for the unit CAB202, Microprocessors and Digital Systems.",
			button: "Update AMS@Home",
			action: "/update",
			feedback: log
		});

		});
	})

module.exports = router;



