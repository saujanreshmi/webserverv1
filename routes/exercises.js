const express = require("express");
const router = express.Router();

router
	.get("/", (req, res) => {
		res.render("topiclist", {
			page: "exercises",
			student: "09003096",
			header: "Weekly Exercises",
			details: "Please select a topic from the list below."
		});
	})

	.get("/TopicIndex", (req, res) => {
		console.log(req);
		res.render("topic", {
			page: "exercises",
			student: "09003096",
			header: "CAB202 - Topic 1: A First C program",
			description: "Getting started, using standard output and the CAB202 ZDK.",
			detail1: "Submissions for assessment items closed at 11:59:59 PM on 31/12/2017.",
			detail2: "Please select an exercise from the list below."
		});		

	})

	.get("/Exercise", (req, res) =>{
		res.send("Exercise " + req.params);
		console.log(req.query);
	})

module.exports = router;