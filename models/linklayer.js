const query = require('./querylayer');
const wait = require('wait.for');

exports.getStudentNumber = function(callback){
	query.USER(function(result){
		var student = '';
	
		if (result == null){
			student = '00000000'
		}
		else{
            var jsonstring = JSON.stringify(result[0]);
            var content = JSON.parse(jsonstring);
			student = content.STUDENTNUMBER
		}
		callback(student);
	})
	
}
	
